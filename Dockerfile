FROM centos
MAINTAINER hxin<chenshun00@gmail.com>
# 把 java 与 tomcat 添加到容器中
ADD jdk-8u171-linux-i586.tar.gz /usr/local
ADD apache-tomcat-9.0.22.tar.gz /data/project/chenshun
ADD load.sh /root
RUN yum -y install vim
    \ yum -y install glibc.i686
ENV MYPATH /usr/local
WORKDIR $MYPATH

# 配置 java 与 tomcat 环境变量
ENV JAVA_HOME /usr/local/jdk1.8.0_171
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV CATALINA_HOME /usr/local/apache-tomcat-9.0.22
ENV CATALINA_BASE /usr/local/apache-tomcat-9.0.22
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin

ENTRYPOINT sh load.sh
