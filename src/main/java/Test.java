import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author chenshun00@gmail.com
 * @since 2019-09-10 23:15
 */
public class Test {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket();
        InetSocketAddress bindAddress = new InetSocketAddress("0.0.0.0", 9999);
        serverSocket.bind(bindAddress);
        while (true) {
            Socket accept = serverSocket.accept();
            InputStream inputStream = accept.getInputStream();
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            java.lang.String s = new java.lang.String(bytes);
            System.out.println("s:" + s);
            OutputStream outputStream = accept.getOutputStream();
            outputStream.write("hello world\n".getBytes("utf-8"));
            int i = 0;
            while (i <= 10) {
                outputStream.write("hello world\n".getBytes("utf-8"));
                i++;
            }
            outputStream.flush();
            outputStream.close();
        }
    }
}
