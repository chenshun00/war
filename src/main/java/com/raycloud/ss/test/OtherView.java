package com.raycloud.ss.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author chenshun00
 * @since 2019/12/27 9:54 PM
 */
@Controller
@RequestMapping("view")
public class OtherView {

    @GetMapping("1")
    public Object result() {
        return "1";
    }

}
