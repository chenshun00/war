package com.raycloud.ss.test;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-13 17:13
 */
@Controller
public class View implements InitializingBean {


    @GetMapping("open")
    public String xx() throws NoSuchMethodException {
        if (1 == 1) {
            throw new NoSuchMethodException("xx");
        }
        return "1";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
//        throw new IllegalArgumentException("x");
    }
}
