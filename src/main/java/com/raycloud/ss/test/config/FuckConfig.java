package com.raycloud.ss.test.config;

import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HandlesTypes;
import java.util.Set;

/**
 * @author chenshun00
 * @since 2019/12/31 11:32 AM
 */
@HandlesTypes(WebApplicationInitializer.class)
public class FuckConfig implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        System.setProperty("spring.profiles.active", ctx.getInitParameter("spring.profiles.active"));
    }
}
