package com.raycloud.ss.test.zz;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author chenshun00
 * @since 2019/12/31 11:53 AM
 */
@Service
public class Good implements InitializingBean {


    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println(System.getProperty("spring.profiles.active"));
    }
}
